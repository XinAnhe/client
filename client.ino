#include <Wire.h>
#include "logos.h"
#include "battery.h"
#include "Adafruit_GFX.h"
#include "Adafruit_SSD1306.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Ticker.h>
#include <AceButton.h>
using namespace ace_button;

#define PIN_RESET 255  //
#define DC_JUMPER 0  // I2C Address: 0 - 0x3C, 1 - 0x3D

//connect to wifi
//const char* ssid = "iot_test";
//const char* password = "iotest@tuvsudcoe";
const char* ssid = "Team SmartBottle";
const char* password = "88323851";
const char* mqtt_server = "192.168.1.100";
String user_name = "OO2tkRAHETAfFhVh6m5V";
String clientID ="Parent Control";

WiFiClient espClient;
PubSubClient psClient(espClient);

const int BUTTON_PIN = D6;
const int ledPin = BUILTIN_LED;

AceButton button(BUTTON_PIN);
void handleEvent(AceButton*, uint8_t, uint8_t);

//////////////////////////////////
// MicroOLED Object Declaration //
//////////////////////////////////
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_MOSI   D7 //Connect to D1 on OLED
#define OLED_CLK    D5 //Connect to D0 on OLED 
#define OLED_DC     D1 //Connect to DC on OLED
#define OLED_CS     D8 //Connect to CS on OLED
#define OLED_RESET  D3 //Connect to RES on OLED
#define SSD1306_128_64
Adafruit_SSD1306 oled(OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);

Ticker timerUpload;
Ticker timerBattery;
Ticker timerWiFi;

boolean iswifi = true;
int timeout = 10;

void displayWifi()
{
  if (iswifi){
    oled.drawBitmap(0, 0,  logo16_wifi_bmp, 16, 16, 1);
  }
  else{
    oled.drawBitmap(0, 0, logo16_Dis_wifi_bmp, 16, 16, 1);
  }
  oled.display();
}

void connectWIFI() {
  delay(5);
  //  WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  int tried = 0;
  while (WiFi.status() != WL_CONNECTED && tried < timeout) {
    delay(1000);
    iswifi = false;
    Serial.print("."); 
    tried++;
  }

  if (WiFi.status() == WL_CONNECTED) {
    iswifi = true;
  }

  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP().toString());
  Serial.println("MAC address: ");
  Serial.println(WiFi.macAddress());
}

void connectMQTT() {
 // Loop until we're reconnected or timeout
 int tried = 0;
 while (!psClient.connected() && tried < timeout) {
   Serial.print("Attempting MQTT connection...");
  
   if (psClient.connect(clientID.c_str(),user_name.c_str(),NULL)) {
      Serial.println("connected");
   } 
   else {
      Serial.print("failed, rc=");
      Serial.print(psClient.state());
      Serial.print("tried: ");
      Serial.print(tried);
      Serial.println(" try again in 1 second");
      // Wait 1 second before retrying
      delay(1000);
      tried++;
  }
 }
}

void initOLED() {
   // These three lines of code are all you need to initialize the
  // OLED and print the splash screen.
  
  // Before you can start using the OLED, call begin() to init
  // all of the pins and configure the OLED.
  oled.begin();
  oled.clearDisplay();
  // To actually draw anything on the display, you must call the
  // display() function. 
  
  //Note: You can change the spalsh screen by editing the array founf in the library source code
  oled.drawBitmap(0, 0, bender, 64, 48, 1);//call the drawBitmap function and pass it the array from above
  oled.display();//display the imgae 
  delay(1000);
}

void setup() {
  Serial.begin(250000);
  pinMode(BUTTON_PIN, INPUT);
  pinMode(ledPin, OUTPUT);
  // set initial state, LED off
  digitalWrite(ledPin, HIGH);

  initOLED();
  
  // Configure the ButtonConfig with the event handler, and enable all higher
  // level events.
  ButtonConfig* buttonConfig = button.getButtonConfig();
  buttonConfig->setEventHandler(handleEvent);
  buttonConfig->setFeature(ButtonConfig::kFeatureClick);
  buttonConfig->setFeature(ButtonConfig::kFeatureDoubleClick);
  buttonConfig->setFeature(ButtonConfig::kFeatureLongPress);

  printText("Connecting to WiFi...");
  connectWIFI();
  psClient.setServer(mqtt_server,1883);
  connectMQTT();
  psClient.setCallback(callback);
  timerBattery.attach(10, displahyBatteryLevel);
  timerWiFi.attach(10, displayWifi);
  timerUpload.attach(30,sendAttr);
  printText("Press & hold button to drink");
}

void handleEvent(AceButton* /* button */, uint8_t eventType,
    uint8_t buttonState) {
  // Print out a message for all events.
  Serial.print(F("handleEvent(): eventType: "));
  Serial.print(eventType);
  Serial.print(F("; buttonState: "));
  Serial.println(buttonState);
  
  switch (eventType) {
    case AceButton::kEventPressed:
      Serial.println("pressed.");
      break;
    case AceButton::kEventReleased:
      Serial.println("released.");
      break;
  }
}

void callback(char* topic, byte* payload, unsigned int length){
  printText("callback here!!!");
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }
}

void printText(String text) {
  oled.clearDisplay();
  oled.setTextSize(0);
  oled.setTextColor(WHITE);
  // Try to set the cursor in the middle of the screen
  oled.setCursor(0, 17);
  // Print the text:
  oled.println(text);
  oled.display();
}

void loop() {
  if (!iswifi) {
    connectWIFI();
  }
  
  if (!psClient.connected()) 
  {
      connectMQTT();
  }
  psClient.loop();

  button.check();

}

void sendAttr() {
  String msg_attribute;

  msg_attribute ="{";  
  msg_attribute += "\"mac\":";
  msg_attribute += "\"" + String(WiFi.macAddress())+ "\"";
        
  msg_attribute += ",";
  msg_attribute += "\"ip\":";
  msg_attribute += "\"" + WiFi.localIP().toString() + "\"";

  msg_attribute += "}";
  psClient.publish("v1/devices/me/attributes" ,msg_attribute.c_str());
}

void displahyBatteryLevel(){
  batteryLevel = getPercentage();
  Serial.print("Current battery level: ");
  Serial.println(batteryLevel);
  if (batteryLevel==0) {
    oled.drawBitmap(100, 0,  logo24_battery0_bmp, 24, 15, 1); 
  }
  else if (batteryLevel > 0 && batteryLevel <=25) {
    oled.drawBitmap(100, 0,  logo24_battery25_bmp, 24, 16, 1);
  }
  else if (batteryLevel>25 && batteryLevel<=50) {
    oled.drawBitmap(100, 0,  logo24_battery50_bmp, 24, 16, 1);
  }
  else if (batteryLevel>50 && batteryLevel<=75) {
    oled.drawBitmap(100, 0,  logo24_battery75_bmp, 24, 16, 1); 
  }
  else if (batteryLevel>75 && batteryLevel<=100) {
    oled.drawBitmap(100, 0,  logo24_battery100_bmp, 24, 16, 1);
  }
  else if (batteryLevel==101) {
    oled.drawBitmap(100, 0,  logo24_batteryCharging_bmp, 24, 16, 1);
  }
  else {
    Serial.print("Battery value invalid");
  }
  oled.display();
}
