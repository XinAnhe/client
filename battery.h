//***********************Battery management****************************
//************************************************************************
//Set A0 as input and add 1M5 resistr or 100k resistor between A0 and 3.3 v+ pin
//Measure the A0 value during the battery life cycle, assume battery charing/discharging follows linear function.
//E.g,
//++++++++++++++++++++++++++ Battery (V)--------------  A0
//++++++++++++++++++++++++++   3.3V     -------------- 254
//++++++++++++++++++++++++++   3.0V     -------------- 200
//++++++++++++++++++++++++++   2.6V     -------------- 100
//************************************************************************
float batteryLevel = 0;
int currentLevel=0;
int lastLevel=1023;

void BatteryInit(){
  pinMode(A0, INPUT);
}

float getVoltage(){
  float raw = analogRead(A0);                                                            
  float volt = map(raw, 206, 315, 270, 420);  //  With a 1M5 resistor
  volt = volt / 100;
  Serial.print("Battery Shiled A0 "); 
  Serial.println(raw);
  Serial.print("Voltage "); 
  Serial.println(volt);
  return volt;
}

float getPercentage(){
  int level=101;
  float raw = analogRead(A0);
  Serial.print("Battery Shiled A0 "); 
  Serial.println(raw);
  
  if (raw >=20 && raw<=80){
    return level;
  }

  currentLevel = map(raw, 206, 315, 0, 100);                //  With a 1M5 resistor
  if (currentLevel <= lastLevel){
    if ( currentLevel < 0 ) {
       currentLevel = 0; 
    }
    else if ( currentLevel > 100 ) {
       currentLevel = 100; 
    }
    lastLevel = currentLevel;
    level = currentLevel;
  }
  else{
    level = lastLevel;
  }
  Serial.print("Battery Percentage: "); 
  Serial.println(level);
  return level;
}
